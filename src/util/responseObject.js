import mapValues from 'lodash/mapValues'
import keyBy from 'lodash/keyBy'

const responseObject = (data) => {
  return mapValues(keyBy(data, '_id'), (value, _id) => {
    console.log('mapping values', _id, value)
    return {
      _id,
      value,
    }
  })
}

export default responseObject
