if(process.NODE_ENV !== 'production') {

  const {createProxyMiddleware} = require('http-proxy-middleware')

  module.exports = function (app) {
    console.log('Setting up proxy for uploads')

    app.use(createProxyMiddleware('/uploads', {target: 'http://localhost:3030/'}))
    app.use(createProxyMiddleware('/api', {target: 'http://localhost:3030/'}))
    app.use(createProxyMiddleware('/authentication', {target: 'http://localhost:3030/'}))
    app.use(createProxyMiddleware('/subscriptions', {target: 'http://localhost:3030/'}))
  }
}