import React from 'react'
import SwipeableRoutes from 'react-swipeable-routes'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import { Route, Link, withRouter } from 'react-router-dom'
import CollectionTab, {
  name as collectionName,
} from '../features/collection/CollectionTab'
import ShareTab, { name as shareName } from '../features/share/ShareTab'
import { UpdateProvider } from '../contexts/UpdateContext'
import SettingsPage from '../features/settings/SettingsPage'
import DocsTab from '../features/docs/DocsTab'

const Main = ({ location, history }) => {
  const tab = location.pathname || location.location.pathname
  // console.log('Main', tab, location)

  const handleTabChange = (event, value) => {
    // console.log('handleTabChange', value)
    history.push(value)
  }

  return (
    <>
      <AppBar position="static">
        <Tabs value={tab} onChange={handleTabChange} variant="fullWidth">
          <Tab label={shareName} value="/" component={Link} to="/" />
          <Tab
            label={collectionName}
            value="/list"
            component={Link}
            to="/list"
          />
          {tab === '/settings' ? (
            <Tab
              label="Settings"
              value="/settings"
              component={Link}
              to="/settings"
            />
          ) : null}
          {tab === '/docs' ? (
            <Tab label="Docs" value="/docs" component={Link} to="/docs" />
          ) : null}
        </Tabs>
      </AppBar>
      <UpdateProvider>
        <SwipeableRoutes axis="x">
          <Route exact path="/" component={ShareTab} />
          <Route exact path="/list" component={CollectionTab} />
          <Route path="/settings" component={SettingsPage} />
          <Route path="/docs" component={DocsTab} />
        </SwipeableRoutes>
      </UpdateProvider>
    </>
  )
}

// use withRouter so that when the location changes, the props update, this rerendering our tabs
export default withRouter(Main)
