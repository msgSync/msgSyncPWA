const eatError = (defaultReturn) => (context) => {
  if (context.error) {
    console.warn('Eating error', context.error)
    context.result = defaultReturn
  }
  return context
}

export default eatError
