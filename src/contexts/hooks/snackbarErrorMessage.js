const snackbarErrorMessage = (enqueueSnackbar) => (context) => {
  enqueueSnackbar(context.error.message, { variant: 'error' })
  return context
}

export default snackbarErrorMessage
