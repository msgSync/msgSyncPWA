import React, { useEffect } from 'react'
import keyBy from 'lodash/keyBy'
import { useSnackbar } from 'notistack'
import { ApiContext } from './ApiContext'
import snackbarErrorMessage from './hooks/snackbarErrorMessage'

const AdminSettingsContext = React.createContext()
const SMTP_SETTINGS = 'smtpSettings'
const FETCH_SETTINGS = 'FETCH_SETTINGS'

const initialState = {
  defaultState: true, // Once this property has been removed the app has successfully fetched the settings from the server
  smtpSettings: {},
}

const reducer = (state, action) => {
  console.log('Admin Settings Reducer', state, action)
  switch (action.type) {
    case SMTP_SETTINGS:
      return {
        ...state,
        smtpSettings: action.payload,
      }
    case FETCH_SETTINGS:
      return action.payload
    default:
      return state
  }
}

const AdminSettingsProvider = ({ children }) => {
  const { adminSettingsApi, updateAdminSetting } = React.useContext(ApiContext)
  const { enqueueSnackbar } = useSnackbar()
  const [adminSettings, dispatch] = React.useReducer(reducer, initialState)

  adminSettingsApi.hooks({
    error: snackbarErrorMessage(enqueueSnackbar),
  })

  // console.log('AdminSettingsProvider', adminSettings)

  // Fetch the admin-settings
  useEffect(() => {
    adminSettingsApi.find().then(({ data }) => {
      dispatch({
        type: FETCH_SETTINGS,
        payload: keyBy(data, '_id'),
      })
    })
  }, [adminSettingsApi])

  const updateSmtpSettings = async (smtpSettings) => {
    console.log('updateSmtpSettings set to ', smtpSettings)
    dispatch({
      type: SMTP_SETTINGS,
      payload: smtpSettings,
    })
    const newAdminSettings = await updateAdminSetting(
      SMTP_SETTINGS,
      smtpSettings
    )
    dispatch(newAdminSettings)
  }

  return (
    <AdminSettingsContext.Provider
      value={{
        adminSettings,
        updateSmtpSettings,
      }}>
      {children}
    </AdminSettingsContext.Provider>
  )
}

const AdminSettingsConsumer = AdminSettingsContext.Consumer

export { AdminSettingsProvider, AdminSettingsConsumer, AdminSettingsContext }
