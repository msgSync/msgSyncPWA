import React, {useState} from 'react'

const UpdateContext = React.createContext()

const UpdateProvider = ({children}) => {

  const [lastUpdate, setUpdate] = useState(new Date())

  const informUpdate = () => {
    console.log('informing update')
    setUpdate(new Date())
  }

  if('serviceWorker' in navigator) {
    window.navigator.serviceWorker.addEventListener('message', event => {
      console.log('Service Worker Message: ', event)
      informUpdate()
    })
  } else {
    console.warn('ServiceWorker not available and unable to update app in real time.')
  }

  return (
    <UpdateContext.Provider
      value={{lastUpdate, informUpdate}}
    >
      {children}
    </UpdateContext.Provider>
  )
};


const UpdateConsumer = UpdateContext.Consumer;

export { UpdateProvider, UpdateConsumer, UpdateContext }