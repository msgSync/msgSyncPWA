import React from 'react'
import Typography from '@material-ui/core/Typography'
import { AppSettingsContext } from '../../contexts/AppSettingsContext'
import Paper from '@material-ui/core/Paper'
import { makeStyles } from '@material-ui/core/styles'
import RegistrationSettings from '../settings/RegistrationSettings'
import PurgeAfterSlider from '../settings/PurgeAfterSlider'
import { AdminSettingsProvider } from '../../contexts/AdminSettingsContext'

const AdminSettings = () => {
  const classes = useStyles()
  const { settings, updatePruneAfterDays } = React.useContext(
    AppSettingsContext
  )

  // console.log('AdminSettings.allowRegistration', settings.allowRegistration)
  return (
    <Paper className={classes.root}>
      <Typography variant="h3" align="center">
        Application Settings
      </Typography>
      <AdminSettingsProvider>
        <RegistrationSettings />
        <PurgeAfterSlider
          appLimit={365}
          value={settings.pruneAfterDays}
          setValue={updatePruneAfterDays}
        />
      </AdminSettingsProvider>
    </Paper>
  )
}

const useStyles = makeStyles({
  root: {
    margin: '2em',
    padding: '1em',
  },
})

export default AdminSettings
