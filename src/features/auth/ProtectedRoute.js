import React from 'react'
import {Redirect, Route} from 'react-router-dom'
import {AuthConsumer} from '../../contexts/AuthContext'

const ProtectedRoute = ({ component: Component, ...rest }) => (
  <AuthConsumer>
    {({ isAuth }) => (
      <Route
        render={
          props =>
            isAuth
            ? <Component {...props} />
            : <Redirect to="/" />
        }
        {...rest}
      />
    )}
  </AuthConsumer>
)

export default ProtectedRoute