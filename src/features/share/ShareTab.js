import React from 'react'
import ShareDrop from './ShareDrop'
import ShareText from './ShareText'

const ShareTab = () => {
    return (
        <>
          <ShareText />
          <ShareDrop />
        </>
    )
}

export const name = 'Share'

export default ShareTab