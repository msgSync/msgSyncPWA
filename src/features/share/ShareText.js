import React from 'react'
import Button from '@material-ui/core/Button'
import { useSnackbar } from 'notistack'
import TextField from '@material-ui/core/TextField'
import { UpdateContext } from '../../contexts/UpdateContext'
import { makeStyles } from '@material-ui/core/styles'
import { ApiContext } from '../../contexts/ApiContext'

let deferredPrompt = null

// Don't show install banner before default case
window.addEventListener('beforeinstallprompt', (event) => {
  console.log('beforeinstallprompt fired')
  event.preventDefault()
  deferredPrompt = event
  return false
})

const ShareText = () => {
  const { enqueueSnackbar } = useSnackbar()
  const classes = useStyles()
  const { sharesApi } = React.useContext(ApiContext)
  const { informUpdate } = React.useContext(UpdateContext)
  const [text, setText] = React.useState('')

  const handleChange = (event) => {
    setText(event.target.value)
  }

  const handleShare = () => {
    console.log('handleShare, posting', text)

    sharesApi
      .create({
        type: 'text',
        text: text,
      })
      .then((res) => {
        console.log('Shares created', res)
        setText('')
        informUpdate()
      })
      .catch((err) => {
        console.error('Error while creating Shares: ', err)
        enqueueSnackbar(`Unable to share: ${err}`, { variant: 'error' })
      })

    //TODO: This should be extracted into it's own hook
    if (deferredPrompt) {
      deferredPrompt.prompt()

      deferredPrompt.userChoice.then(function (choiceResult) {
        console.log(choiceResult.outcome)

        if (choiceResult.outcome === 'dismissed') {
          console.log('User cancelled installation')
        } else {
          console.log('User added to home screen')
        }
      })

      deferredPrompt = null
    }
  }

  return (
    <div className={classes.root}>
      <TextField
        className={classes.textField}
        label="Share"
        multiline
        value={text}
        onChange={handleChange}
        helperText="Share url or text"
        variant="outlined"
      />
      <Button variant="contained" onClick={handleShare}>
        Share
      </Button>
    </div>
  )
}

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    alignItems: 'baseline',
    alignContent: 'center',
  },
  textField: {
    width: '50%',
    marginLeft: '25%',
    marginTop: '2em',
    marginBottom: '2em',
  },
}))

export default ShareText
