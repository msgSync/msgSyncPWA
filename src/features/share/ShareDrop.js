import React, { useState } from 'react'
import { useSnackbar } from 'notistack'
import { makeStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import Dropzone from 'react-dropzone'
import LinearProgress from '@material-ui/core/LinearProgress'
import { UpdateContext } from '../../contexts/UpdateContext'
import { AuthContext } from '../../contexts/AuthContext'

const ShareDrop = () => {
  const classes = useStyles()
  const { enqueueSnackbar } = useSnackbar()
  const { informUpdate } = React.useContext(UpdateContext)
  const { accessToken } = React.useContext(AuthContext)
  const [isUploading, setUploading] = useState(false)

  const onDrop = (acceptedFiles, rejectedFiles) => {
    setUploading(true)
    console.log('my ondrop function', acceptedFiles, rejectedFiles)

    acceptedFiles.forEach((file) => {
      const reader = new FileReader()
      reader.onload = () => {
        const fileAsDataUrl = reader.result
        // do whatever you want with the file content
        console.log('fileAsDataUrl ', file)

        const body = new FormData()
        body.append('text', file.name)
        body.append('fileType', file.type)
        body.append('uri', fileAsDataUrl)

        fetch('/api/files', {
          method: 'POST',
          headers: {
            // 'Content-Type': 'multipart/form-data',
            Authorization: `Bearer ${accessToken}`,
          },
          body,
        })
          .then((res) => {
            console.log('files drop response', res)
            setUploading(false)
            informUpdate()
          })
          .catch((err) => {
            setUploading(false)
            enqueueSnackbar(`Unable to share: ${err.message}`, {
              variant: 'error',
            })
          })
      }
      reader.onabort = () => {
        console.log('file reading was aborted')
        setUploading(false)
      }
      reader.onerror = () => {
        console.log('file reading has failed')
        setUploading(false)
      }

      reader.readAsDataURL(file)
      // reader.readAsBinaryString(file)
    })
  }

  return (
    <Paper className={classes.paper}>
      {isUploading ? (
        <LinearProgress />
      ) : (
        <Dropzone onDrop={onDrop}>
          {({ getRootProps, getInputProps, isDragActive }) => (
            <div
              {...getRootProps()}
              style={{
                padding: '1em',
                borderWidth: '2px',
                borderRadius: '7px',
                borderStyle: isDragActive ? 'solid' : 'dashed',
              }}>
              <input {...getInputProps()} />
              {isDragActive ? (
                <p>Drop files here...</p>
              ) : (
                <p>Drop files here, or click to select files to upload.</p>
              )}
            </div>
          )}
        </Dropzone>
      )}
    </Paper>
  )
}

const useStyles = makeStyles((theme) => ({
  paper: {
    width: '50%',
    marginLeft: '25%',
    marginTop: '2em',
    marginBottom: '2em',
    padding: '2em',
  },
}))

export default ShareDrop
