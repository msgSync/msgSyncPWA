import React, { useContext } from 'react'
import SwaggerUI from 'swagger-ui-react'
import 'swagger-ui-react/swagger-ui.css'
import { AuthContext } from '../../contexts/AuthContext'

const DocsTab = () => {
  const { accessToken } = useContext(AuthContext)

  return (
    <>
      <SwaggerUI
        url="/api"
        persistAuthorization={true}
        requestInterceptor={(request) => {
          request.headers.Authorization = accessToken
          // console.log('requestInterceptor', request)
          return request
        }}
      />
    </>
  )
}

export default DocsTab
