import React from 'react'
import { AppSettingsContext } from '../../contexts/AppSettingsContext'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Switch from '@material-ui/core/Switch'
import EmailSettingsForm from './EmailSettingsForm'

const RegistrationSettings = () => {
  const { settings, updateRegistration } = React.useContext(AppSettingsContext)
  // console.log('Registration Settings', settings.allowRegistration)
  return (
    <>
      <FormControlLabel
        control={
          <Switch
            checked={settings.allowRegistration}
            onChange={(event) => updateRegistration(event.target.checked)}
            name="allowRegistration"
            color="primary"
          />
        }
        label="Allow Registration"
      />
      {settings.allowRegistration ? (
        <>
          <EmailSettingsForm />
        </>
      ) : null}
    </>
  )
}

export default RegistrationSettings
