import React from 'react'
import Typography from '@material-ui/core/Typography'
import Slider from '@material-ui/core/Slider'
import Input from '@material-ui/core/Input'

const PurgeAfterSlider = ({ value, appLimit, setValue }) => {
  const handleSliderChange = (event, newValue) => {
    setValue(newValue)
  }

  const handleInputChange = (event) => {
    setValue(event.target.value === '' ? '' : Number(event.target.value))
  }

  const handleBlur = () => {
    if (value < 1) {
      setValue(1)
    } else if (value > appLimit) {
      setValue(appLimit)
    }
  }

  // console.log('PurgeAfterSlider.userSetting', userSetting)

  return (
    <>
      <Typography id="input-slider" gutterBottom variant="subtitle2">
        Prune Shares older than{' '}
        <Input
          // className={classes.input}
          value={value}
          margin="dense"
          onChange={handleInputChange}
          onBlur={handleBlur}
          inputProps={{
            step: 1,
            min: 1,
            max: appLimit,
            type: 'number',
            'aria-labelledby': 'input-slider',
          }}
        />{' '}
        days
      </Typography>
      <Slider
        value={typeof value === 'number' ? value : 0}
        onChange={handleSliderChange}
        aria-labelledby="input-slider"
        min={1}
        max={appLimit}
      />
    </>
  )
}

export default PurgeAfterSlider
