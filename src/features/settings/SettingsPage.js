import React from 'react'
import Typography from '@material-ui/core/Typography'
import { AuthContext } from '../../contexts/AuthContext'
import AdminSettings from '../admin/AdminSettings'
import { makeStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import { UserSettingsContext } from '../../contexts/UserSettingsContext'
import { AppSettingsContext } from '../../contexts/AppSettingsContext'
import PurgeAfterSlider from './PurgeAfterSlider'

const SettingsPage = () => {
  const classes = useStyles()
  const { isAdmin } = React.useContext(AuthContext)
  const { userSettings, setPruneAfterDays } = React.useContext(
    UserSettingsContext
  )
  const { settings } = React.useContext(AppSettingsContext)

  return (
    <>
      <Paper className={classes.root}>
        <Typography variant="h3" align="center">
          User Settings
        </Typography>
        {/*<Typography variant="caption">*/}
        {/*  {JSON.stringify(userSettings)}*/}
        {/*</Typography>*/}
        {settings.pruneAfterDays > 1 ? (
          <PurgeAfterSlider
            appLimit={settings.pruneAfterDays}
            value={userSettings.pruneAfterDays}
            setValue={setPruneAfterDays}
          />
        ) : null}
      </Paper>
      {isAdmin ? <AdminSettings /> : null}
    </>
  )
}

const useStyles = makeStyles({
  root: {
    margin: '2em',
    padding: '1em',
  },
})

export default SettingsPage
