import React from 'react'
import { Formik } from 'formik'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import { makeStyles } from '@material-ui/core/styles'
import { AdminSettingsContext } from '../../contexts/AdminSettingsContext'
import Switch from '@material-ui/core/Switch'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import defaultTo from 'lodash/defaultTo'

const EmailSettingsForm = () => {
  const classes = useStyles()
  const { adminSettings, updateSmtpSettings } = React.useContext(
    AdminSettingsContext
  )
  // console.log('EmailSettingsForm', adminSettings.smtpSettings)

  return (
    <div className={classes.container}>
      <Formik
        enableReinitialize
        initialValues={{
          host: defaultTo(adminSettings?.smtpSettings?.host, ''),
          port: defaultTo(adminSettings?.smtpSettings?.port, ''),
          secure: defaultTo(adminSettings?.smtpSettings?.secure, true),
          user: defaultTo(adminSettings?.smtpSettings?.user, ''),
          pass: defaultTo(adminSettings?.smtpSettings?.pass, ''),
        }}
        onSubmit={(
          { host, port, secure, user, pass },
          { setSubmitting, setStatus, setErrors }
        ) => {
          // console.log('EmailSettingsForm.onSubmit', host, port, secure, user, pass)
          updateSmtpSettings({
            host,
            port,
            secure,
            user,
            pass,
          })
        }}>
        {({ values, errors, status, handleSubmit, handleChange }) => (
          <form onSubmit={handleSubmit}>
            <div className={classes.form}>
              <TextField
                name="host"
                value={values.host}
                label="SMTP Hostname"
                onChange={handleChange}
                error={Boolean(status)}
                helperText={status}
              />
              <TextField
                name="port"
                value={values.port}
                label="SMTP Port"
                onChange={handleChange}
              />
              <FormControlLabel
                control={
                  <Switch
                    checked={values.secure}
                    onChange={handleChange}
                    name="secure"
                    color="primary"
                  />
                }
                label="Secure SMTP Port"
              />
              <TextField
                name="user"
                value={values.user}
                label="SMTP Username"
                onChange={handleChange}
              />
              <TextField
                name="pass"
                value={values.pass}
                label="SMTP Password"
                type="password"
                onChange={handleChange}
              />

              <div className={classes.buttonContainer}>
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  className={classes.testBtn}>
                  Save SMTP Settings
                </Button>

                {/*<Button*/}
                {/*  variant="outlined"*/}
                {/*  color="inherit"*/}
                {/*  className={classes.testBtn}>*/}
                {/*  Test Email*/}
                {/*</Button>*/}
              </div>
            </div>
          </form>
        )}
      </Formik>
    </div>
  )
}

const useStyles = makeStyles({
  container: {
    marginLeft: '2em',
  },
  form: {
    // padding: 20,
    // overflow: 'auto',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'flex-start',
  },
  buttonContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    // marginLeft: '-2em',
  },
  testBtn: {
    margin: '1em',
  },
})

export default EmailSettingsForm
