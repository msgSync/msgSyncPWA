import React from 'react'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import DownloadIcon from '@material-ui/icons/CloudDownload'
import { withRouter } from 'react-router-dom'
import DeleteItemAction from './DeleteItemAction'

const ItemFile = ({ share }) => {
  return (
    <ListItem
      button
      // onClick={handleClick}
      component="a"
      href={share.url}
      target="_blank"
      rel="noopener noreferrer"
      download={share.text}>
      <ListItemIcon>
        <DownloadIcon />
      </ListItemIcon>
      <ListItemText primary={share.text} secondary={share.createdAt} />

      <DeleteItemAction share={share} />
    </ListItem>
  )
}

export default withRouter(ItemFile)
