import React from 'react'
import CollectionContainer from './CollectionContainer'
import {UpdateConsumer} from '../../contexts/UpdateContext'

const CollectionTab = () => {
    return (
        <div>
          <UpdateConsumer>
            {({lastUpdate}) => {
              return <CollectionContainer lastUpdate={lastUpdate} />
            }}
          </UpdateConsumer>
        </div>
    )
}

export const name = 'My Collection'

export default CollectionTab