import React from 'react'
import {withStyles} from '@material-ui/core/styles'
import List from '@material-ui/core/List'
import remove from 'lodash/remove'
import ItemText from './ItemText'
import ItemFile from './ItemFile'

const CollectionList = ({classes, shares, lastUpdate}) => {
  // console.log('shares list', shares)

  return (
    <div className={classes.root}>
      <List>
        {
          shares.map( share => {
            share.delete = () => remove(shares, s => s.id === share.id)

            return share.type === 'file'
              ? <ItemFile key={share.id} share={share} />
              : <ItemText key={share.id} share={share}/>
          })
        }
      </List>
    </div>
  )
}

const style = {
  root: {
    margin: '1em'
  }
}

export default withStyles(style)(CollectionList)