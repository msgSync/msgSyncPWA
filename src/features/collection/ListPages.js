import React from 'react'

const ListPages = ({currentPage, amountOfPages}) => {

  let pages = []

  for(let p = 1; p <= amountOfPages; p++) {
    pages.push(p)
  }

  return(
    <div>
      {
        pages.map(page => (
          <p>{page}</p>
        ))
      }
    </div>
  )
}

export default ListPages