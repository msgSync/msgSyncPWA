import React from 'react'
import IconButton from '@material-ui/core/IconButton'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import DeleteIcon from '@material-ui/icons/Delete'
import { UpdateConsumer } from '../../contexts/UpdateContext'
import { ApiContext } from '../../contexts/ApiContext'

const DeleteItemAction = ({ share }) => {
  const { sharesApi } = React.useContext(ApiContext)

  const handleClick = (informUpdate) => () => {
    console.log('deleting id', share.id)
    sharesApi
      .remove(share.id)
      .then((response) => {
        share.delete()
        informUpdate()
      })
      .catch((err) => {
        console.log('delete failed')
        informUpdate()
      })
  }

  return (
    <ListItemSecondaryAction>
      <UpdateConsumer>
        {({ informUpdate }) => (
          <IconButton aria-label="Delete" onClick={handleClick(informUpdate)}>
            <DeleteIcon />
          </IconButton>
        )}
      </UpdateConsumer>
    </ListItemSecondaryAction>
  )
}

export default DeleteItemAction
