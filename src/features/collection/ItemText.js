import React from 'react'
import LinkIcon from '@material-ui/icons/Link'
import copyToClipboard from 'copy-to-clipboard'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import DeleteItemAction from './DeleteItemAction'

const ItemText = ({share}) => {

  const handleClick = () => {
    console.log('text copied to clipboard', share.text)
    copyToClipboard(share.text)
  }

  return (
    <ListItem
      key={share.id}
      button
      onClick={handleClick}
    >
      <ListItemIcon>
        <LinkIcon/>
      </ListItemIcon>
      <ListItemText
        primary={share.text}
        secondary={share.createdAt}
      />

      <DeleteItemAction share={share} />
    </ListItem>
  )
}

export default ItemText