import React, { useEffect, useState } from 'react'
import uniqBy from 'lodash/uniqBy'
import maxBy from 'lodash/maxBy'
import prettyDate from 'pretty-date'
import Button from '@material-ui/core/Button'
import MoreHoriz from '@material-ui/icons/MoreHoriz'
import CollectionList from './CollectionList'
import { makeStyles } from '@material-ui/core/styles'
import { ApiContext } from '../../contexts/ApiContext'

const prepareShare = (data, list = []) => {
  for (let share of data) {
    // console.log(share)
    list.push({
      id: share._id,
      type: share.type,
      text: share.text,
      url: share.file,
      createdAt: prettyDate.format(new Date(share.createdAt)),
    })
  }

  return list
}

const CollectionContainer = ({ lastUpdate }) => {
  const classes = useStyles()
  const { sharesApi } = React.useContext(ApiContext)
  const [shares, setShares] = useState([])
  const [latestShare, setLatest] = useState(new Date(0).toISOString())
  const [showMore, setMore] = useState(true)

  if('serviceWorker' in navigator){
    console.log('we have a service worker in collection container')
    const updatesChannel = new BroadcastChannel('api-update')
    updatesChannel.addEventListener("message", async (event) => {
      const { cacheName, updatedURL } = event.data.payload;
      console.log('cache - name', cacheName)
      console.log('cache - updateUrl', updatedURL)
      const cache = await caches.open(cacheName);
      const updatedResponse = await cache.match(updatedURL);
      console.log('cache - updatedResponse')
      if (updatedResponse && cacheName === "movies") {
        const shares = await updatedResponse.json();
        setShares(shares);
      }
    });
  }

  const fetchMoreShares = async () => {
    console.log('fetchMoreShares')

    try {
      const result = await sharesApi.find({
        query: {
          $sort: { createdAt: '-1' },
          $skip: shares.length,
        },
      })

      console.log('collectionList', result)

      // result.data = reverse(sortBy(result.data, 'createdAt'))
      const list = prepareShare(result.data)

      let fetchedShares = uniqBy([...shares, ...list], 'id')

      setShares(fetchedShares)
      console.log(
        'more pages?',
        result.total > fetchedShares.length,
        'skip',
        fetchedShares.length
      )
      setMore(result.total > fetchedShares.length)
    } catch (error) {
      console.log('CollectionList fetchMoreShares Error', error)
    }
  }

  const fetchRecentShares = React.useCallback(async () => {
    try {
      const result = await sharesApi.find({
        query: {
          $sort: { createdAt: '-1' },
          createdAt: { $gt: latestShare },
        },
      })

      if (result.total > 0) {
        console.log('latest', maxBy(result.data, 'createdAt'))
        setLatest(maxBy(result.data, 'createdAt').createdAt)
      }

      const list = prepareShare(result.data)
      setShares(uniqBy([...list, ...shares], 'id'))
    } catch (error) {
      console.log('CollectionList fetchInitialShares Error', error)
    }
  }, [shares, latestShare, sharesApi])

  useEffect(() => {
    fetchRecentShares()
  }, [lastUpdate])

  return (
    <div className={classes.root}>
      <CollectionList shares={shares} />
      {showMore ? (
        <div className={classes.showMore}>
          <Button variant="outlined" onClick={fetchMoreShares}>
            show more <MoreHoriz />
          </Button>
        </div>
      ) : null}
    </div>
  )
}

const useStyles = makeStyles((theme) => ({
  root: {
    margin: '1em',
  },
  showMore: {
    margin: 'auto',
    textAlign: 'center',
  },
}))

export default CollectionContainer
